# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Konovalov Petr

E-MAIL: pkonjob@yandex.ru

# SOFTWARE

* JDK 1.8.0

* Windows

# RUN PROGRAM

```
java -jar ./task-manager.jar
```
